<?php

namespace App\Helpers;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CSVHelper
{

    public static function download(array $array)
    {

        $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=demo.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];
        
        # add headers for each column in the CSV download
        array_unshift($array, array_keys($array[0]));

        $callback = function() use ($array) 
        {
            $handle = fopen('php://output', 'w');
            foreach ($array as $row) { 
                fputcsv($handle, $row);
            }
            fclose($handle);
        };

        return response()->stream($callback, 200, $headers);
    }
}